FROM scratch

COPY conf /etc/mentat
COPY lib /usr/local/bin
COPY trustmodule/protective_trustmodule /tmp/trustmodule/protective_trustmodule
COPY trustmodule/setup.py /tmp/trustmodule
