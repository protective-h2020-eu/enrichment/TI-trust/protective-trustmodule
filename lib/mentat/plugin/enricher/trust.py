#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Enricher plugin for trust computation via trust module.

"""

__author__ = "Samuel Ruppert <samuel.ruppert@stud.tu-darmstadt.de>"
__credits__ = "Sheikh Mahbub Habib <sheikh@tk.tu-darmstadt.de>, Emmanouil Vasilomanolakis <vasilomano@tk.tu-darmstadt.de>"

#
# Custom libraries
#
import mentat.const
import mentat.plugin.enricher
from protective_trustmodule import AlertQuality, AlertQualityConfig, NerdClient, NerdClientConfig


class TrustEnricherPlugin(mentat.plugin.enricher.EnricherPlugin):
    """
    Simple implementation of enricher plugin using the trust module.
    """

    def setup(self, daemon, config):
        """
        Perform setup of enricher plugin.
        """
        daemon.logger.debug(
            "Initialized '{}' enricher plugin".format(self.__class__.__name__))

        # init alert quality computation model
        # if a custom configuration is set in the mentat-enricher.py.conf file
        # override defaults with the custom configuration
        if isinstance(config, dict) and isinstance(config.get("alertquality_config"), dict):
            alertquality_cfg = AlertQualityConfig(**config["alertquality_config"])
        else:
            alertquality_cfg = AlertQualityConfig()

        self.alert_quality = AlertQuality(alertquality_cfg)

        # init NERD client for entity reputation
        # if a custom configuration is set in the mentat-enricher.py.conf file
        # override defaults with the custom configuration
        if isinstance(config, dict) and isinstance(config.get("nerdapi_config"), dict):
            nerd_cfg = NerdClientConfig(**config["nerdapi_config"])
        else:
            nerd_cfg = NerdClientConfig()
            
        self.nerd_client = NerdClient(nerd_cfg)

    def process(self, daemon, message_id, message):
        """
        Process given message.
        """

        changed = self.FLAG_UNCHANGED
        msg = message.to_json()

        # calculate alert quality score
        try:
            quality = self.alert_quality.get_quality(msg)
        except Exception as e:
            daemon.logger.error("TrustEnricherPlugin_AlertQuality ERROR: Computing alert quality failed for message '{}': {}".format(message_id, str(e)))
        else:
            daemon.logger.info("TrustEnricherPlugin_AlertQuality INFO: Score {}, IP recurrence {}".format(quality['Score'], quality['IpRecurrence']))
            message[self.alert_quality.cfg.IDEA_ALERT_QUALITY] = quality
            changed = self.FLAG_CHANGED

        # retrieve entity reputation score
        try:
            nerd_score = self.nerd_client.score(msg)
        except Exception as e:
            daemon.logger.error("TrustEnricherPlugin_NerdClient ERROR: Retrieving entity reputation failed for message '{}': {}".format(message_id, str(e)))
        else:
            daemon.logger.info("TrustEnricherPlugin_NerdClient INFO: Entity reputation {}".format(nerd_score))
            message[self.nerd_client.cfg.IDEA_ENTITY_REPUTATION] = nerd_score
            changed = self.FLAG_CHANGED

        return (daemon.FLAG_CONTINUE, changed)
