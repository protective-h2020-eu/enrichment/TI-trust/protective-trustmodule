#!/bin/bash

docker build -t protective-trustmodule .
docker tag protective-trustmodule registry.gitlab.com/protective-h2020-eu/enrichment/ti-trust/protective-trustmodule:1.2.1
docker push registry.gitlab.com/protective-h2020-eu/enrichment/ti-trust/protective-trustmodule:1.2.1

docker build -t protective-mentat-trustmodule -f Dockerfile_Mentat-trustmodule .
docker tag protective-mentat-trustmodule registry.gitlab.com/protective-h2020-eu/enrichment/ti-trust/protective-trustmodule/protective-mentat-trustmodule:1.2.1
docker push registry.gitlab.com/protective-h2020-eu/enrichment/ti-trust/protective-trustmodule/protective-mentat-trustmodule:1.2.1