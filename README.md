# Trust Module

This is the dockerized trust computation module along with the TrustEnricher plugin to be used in the Mentat pipeline of PROTECTIVE.

# Installation

You can build the docker images as follows:

1. `git clone --recursive https://gitlab.com/protective-h2020-eu/enrichment/TI-trust/protective-trustmodule.git`
2. `cd protective-trustmodule`
3. Configure the TrustEnricher plugin in file `conf/mentat-enricher.py.conf`: 

    ```python
    ...
    {
        "name":  "mentat.plugin.enricher.trust.TrustEnricherPlugin",
        "alertquality_config": {
            "IDEA_ALERT_QUALITY": "AlertQuality",
            "IPR_SKETCH_FILE": "ipr_cmsketch.bin" # ip recurrence data will be stored in this file
            "ST_MOVING_AVG_FILE": "st_movavg.bin" # source relevance data will be stored in this file 
        },
        "nerdapi_config": {
            "IDEA_ENTITY_REPUTATION": "EntityReputation",
            "API_TOKEN": "<NERD API TOKEN>" # replace with your NERD API token
        }
    }
    ...
    ```
    A full list of configuration settings can be found in the [TrustModule's README](https://gitlab.com/protective-h2020-eu/enrichment/TI-trust/trustmodule).
    
    ### Important configuration

    * `IDEA_ALERT_QUALITY`, `IDEA_ENTITY_REPUTATION`:
    These settings determine the fields in the IDEA alert in which the TrustEnricher's results are added. (see the Result section) They can be omitted, but be careful when changing them since other modules depend on the default names. 

    * `IPR_SKETCH_FILE`, `ST_MOVING_AVG_FILE`:
    Please note that you should choose (absolute) file paths to a persistent docker volume when running the container to prevent data loss. The volume can be specified in the respective docker-compose file of the [Protective Node Repository](https://gitlab.com/protective-h2020-eu/protective-node). 

    * `API_TOKEN`: 
    You can retrieve your NERD API access token from the account page of the [NERD API](https://nerd.cesnet.cz/).

4. `bash ./build.sh`

Afterwards you can run Mentat with the newly built TrustEnricher plugin via
`docker-compose up -d`

# Test
After bringing up the docker containers you can generate some alerts to test if it's working:

`docker exec -it mentat_server_1 bash`

`mentat-ideagen.py --count 100`

There should be some log messages in `/var/mentat/log/mentat-enricher.py.log` printing the overall alert quality score and ip recurrence.

If you check the database all alerts should have two additional fields as explained in the next section.

# Result

The computed results are appended to the alert so that it gets stored in the database eventually:

```python
{
    ...

    "AlertQuality": {
        "Score": <float>,
        "Inputs": {
            "Completeness": <float>,
            "SourceRelevance": <float>,
            "AlertFreshness": <float>
        },
        "Opinion": {
            "Quality": <float>,
            "Certainty": <float>,
            "SourceTrustworthiness": <float>
        },
        "IpRecurrence": <int>
    },

    ...
    
    "EntityReputation": <float>

    ...
}
```